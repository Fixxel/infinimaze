using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibles : MonoBehaviour
{

    public List<GameObject> ThisType = new List<GameObject>();
    private int thisValue;
    private bool healthPickup;
    // Start is called before the first frame update
    void Start()
    {

        //99% chance to be a valuable
        if(Random.value < 0.99){
            thisValue = 1;
            ThisType[1].SetActive(true);

            if(Random.value < 0.25){ //25% chance to be more valuable
                thisValue = thisValue * 2;
                ThisType[2].SetActive(true);

                if(Random.value < 0.15){ //15% chance to be more valuable
                    thisValue = thisValue * 2;
                    ThisType[3].SetActive(true);

                }
                    if(Random.value < 0.1){ //10% chance to be more valuable
                        thisValue = thisValue * 2;
                        ThisType[4].SetActive(true);

                    }
            }
        }else { //1% chance to be health pickup
            healthPickup = true;
            ThisType[0].SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<Player>()){
            
            if(!healthPickup)other.GetComponent<Player>().collectedCoins += thisValue;
            else{
                other.GetComponent<Player>().HP++;
            }

            Destroy(gameObject);
        }
    }
}
