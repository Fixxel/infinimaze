using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollForGobj : MonoBehaviour
{

    public List<GameObject> GoList = new List<GameObject>();

    void Start()
    {
        int roll = Random.Range(0, GoList.Count);
        Debug.Log(roll);
        GoList[roll].SetActive(true);
    }
}
