using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeRandomRot : MonoBehaviour
{

    //adding more randomization to cubes, mainly floor cubes
    void Start()
    {
        int roll = Random.Range(0, 6);

        switch (roll){
            case 0:
                transform.Rotate(90, 0, 0);
                break;
            case 1:
                transform.Rotate(-90, 0, 0);
                break;
            case 2:
                transform.Rotate(0, 90, 0);
                break;
            case 3:
                transform.Rotate(0, -90, 0);
                break;
            case 4:
                transform.Rotate(0, 0, 90);
                break;
            case 5:
                transform.Rotate(0, 0, -90);
                break;
            case 6:
                break;
        }
    }
}
