using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour
{
    public bool ConstantRotation;
    public bool ConstantLimitX;
    public bool ConstantLimitY;
    public bool ConstantLimitZ;
    public float ConstantRotationSpeed;
    public float MinDegrees;
    public float MaxDegrees;
    // Start is called before the first frame update
    void Start()
    {
        transform.Rotate(Random.Range(MinDegrees, MaxDegrees), Random.Range(MinDegrees, MaxDegrees), Random.Range(MinDegrees, MaxDegrees));
    }

    private void Update() {
        if(ConstantRotation){
            if(!ConstantLimitX) transform.Rotate(ConstantRotationSpeed * Time.deltaTime, 0, 0);
            if(!ConstantLimitY) transform.Rotate(0, ConstantRotationSpeed * Time.deltaTime, 0);
            if(!ConstantLimitZ) transform.Rotate(0, 0, ConstantRotationSpeed * Time.deltaTime);
        }
    }
}
