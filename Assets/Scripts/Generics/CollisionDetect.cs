using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetect : MonoBehaviour
{
    public bool collisionDetected;
    public void OnTriggerStay(Collider other) {
        if(other.GetComponentInParent<Blocks>())collisionDetected = true;
    }

    private void OnTriggerExit(Collider other) {
        collisionDetected = false;
    }
}
