using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public bool OnGround;
    private bool OrientationCornered;
    private bool ProjectileCooldown;
    private int rotationCount;
    public int collectedCoins;

    public GameObject SensorForwardRef;
    public GameObject SensorBackRef;
    public GameObject SensorLeftRef;
    public GameObject SensorRightRef;
    public GameObject CamRef;
    public GameObject LevelRef;

    public GameObject ProjectileRef;
    public GameObject LastScoreRef;

    public int HP;
    public int startHP;

    private void Start() {
        HP = startHP;
    }
    void Update()
    {
        //forward and backward functionality
        if(!SensorForwardRef.GetComponent<CollisionDetect>().collisionDetected && OnGround){
            if((Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.W))){
                if(!OrientationCornered){
                    transform.Translate(0, 0, 1f);
                    transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z)); //in case player goes off grid center
                    LevelRef.GetComponent<Level>().WorldTick();

                    ProjectileCooldown = false;
                }
                else if(OrientationCornered){
                    transform.Translate(0, 0, 1.5f);
                    transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z)); //in case player goes off grid center
                    LevelRef.GetComponent<Level>().WorldTick();
                    
                    ProjectileCooldown = false;
                }

            }
        }
        if(!SensorBackRef.GetComponent<CollisionDetect>().collisionDetected && OnGround){
            if((Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.S))){
                if(!OrientationCornered){
                    transform.Translate(0, 0, -1f);
                    transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z)); //in case player goes off grid center
                    LevelRef.GetComponent<Level>().WorldTick();

                    ProjectileCooldown = false;
                }
                else if(OrientationCornered){
                    transform.Translate(0, 0, -1.5f);
                    transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z)); //in case player goes off grid center
                    LevelRef.GetComponent<Level>().WorldTick();

                    ProjectileCooldown = false;
                }
            }
        }
        
        //rotation functionality
        if(Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.A)){
            transform.Rotate(0, -45f, 0);
            OrientationCornered = !OrientationCornered;

            ProjectileCooldown = false;

            if(rotationCount < 1)rotationCount++; //making world tick when player has rotated twice
                else{
                    LevelRef.GetComponent<Level>().WorldTick();
                    rotationCount = 0;
                }
        }else if(Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.D)){
            transform.Rotate(0, 45f, 0);
            OrientationCornered = !OrientationCornered;

            ProjectileCooldown = false;

            if(rotationCount < 1)rotationCount++; //making world tick when player has rotated twice
                else{
                    LevelRef.GetComponent<Level>().WorldTick();
                    rotationCount = 0;
                }
        }

        //stay in place and wait for world tick
        if(Input.GetKeyDown(KeyCode.Space)){
            LevelRef.GetComponent<Level>().WorldTick();
            ProjectileCooldown = false;
        }

        //firing projectile
        if(!ProjectileCooldown && Input.GetKeyDown(KeyCode.F)){
            ProjectileCooldown = true;

            GameObject projectile = Instantiate(ProjectileRef, transform.position, transform.rotation);
            projectile.transform.Translate(0, 0, 1);
            if(OrientationCornered) projectile.transform.Translate(0, 0, 0.5f); //correction for cornered shot

            projectile.transform.SetParent(LevelRef.transform);
            projectile.GetComponent<Projectile>().DMG = 1; //assigning this here, gotta rework it later

            LevelRef.GetComponent<Level>().ProjectilesToTick.Add(projectile);
        }


        //falling off level
        if(transform.position.y < -10){
            LoseHP(1);
            LevelRef.GetComponent<Level>().NextLevel();
        }

        //DevStuff
/*         if(Input.GetKeyDown(KeyCode.Minus)){
            LevelRef.GetComponent<Level>().NextLevel();
        } */
    }

    public void LoseHP(int amount){
        HP -= amount;
        if(HP <= 0){ //check if game over
            LastScoreRef.GetComponent<LastScore_listener>().calculateScore();
            LevelRef.GetComponent<Level>().NewGameReset();
            collectedCoins = 0;
            HP = startHP;
        }
    }

    private void OnTriggerStay(Collider other) {
        OnGround = true;
    }

    private void OnTriggerExit(Collider other) {
        OnGround = false;
    }
}
