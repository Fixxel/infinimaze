using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blocks : Level
{

    public bool starterBlock;
    public bool SeeThrough;
    public GameObject FloorRef;
    public GameObject WallRef;


    public GameObject CollectibleRef;
    
    // Start is called before the first frame update
    void Start()
    {
        SeeThrough = true;
        
        if(!starterBlock){
            if(Random.value < 0.90){
                FloorRef.SetActive(true);
                SeeThrough = true;
            }else{
                WallRef.SetActive(true);
                SeeThrough = false;
            }
        }else{  //always make floor if this is starterblock where player spawns
            FloorRef.SetActive(true);
        }


        //spawning collectibles
        if(Random.value > 0.5 && !starterBlock){
            GameObject collectible = Instantiate(CollectibleRef, transform.position, transform.rotation);
            collectible.transform.Translate(0, 0.75f, 0);

            collectible.transform.SetParent(this.gameObject.transform);
        }
    }
}
