using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSourceRotation : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(Vector3.down * 20f * Time.deltaTime);
    }
}
