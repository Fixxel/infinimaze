using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public GameObject BlockRef;
    public GameObject EntityRef;
    public GameObject PlayerRef;
    public int LevelNum;
    public int BlockCount;
    public int EntityCount;
    public List<GameObject> EntitiesToTick = new List<GameObject>();
    public List<GameObject> ProjectilesToTick = new List<GameObject>();

    private List<Vector3> blockPosList = new List<Vector3>();
    private List<Vector3> entityPosList = new List<Vector3>();
    
    // Start is called before the first frame update
    void Start()
    {
        CreateLevel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //this happens everytime player moves
    public void WorldTick(){
        foreach(GameObject gObj in EntitiesToTick){
            if(gObj != null) gObj.GetComponent<Entity>().Tick();
        }
        foreach(GameObject gObj in ProjectilesToTick){
            if(gObj != null) gObj.GetComponent<Projectile>().Tick();
        }
    }


    //resetting to level 0 without needing to reload scene (reloading scene messes up with postprocessing)
    public void NewGameReset(){
        RepositonPlayer();

        LevelNum = 0;

        EntitiesToTick.Clear();
        ProjectilesToTick.Clear();
        blockPosList.Clear();
        entityPosList.Clear();

        //rolling next level block count
        BlockCount = Random.Range(25, 100);
        EntityCount = Random.Range(2, 5);

        //removing children
        foreach (Transform child in gameObject.transform) {
            Destroy(child.gameObject);
        }

        CreateLevel();
    }



    //progressing to next level
    public void NextLevel(){
        RepositonPlayer();

        LevelNum++;

        //clearing lists
        EntitiesToTick.Clear();
        ProjectilesToTick.Clear();
        blockPosList.Clear();
        entityPosList.Clear();

        //rolling next level block count
        BlockCount = Random.Range(25, 100);
        EntityCount = Random.Range(2, 5);

        //removing children
        foreach (Transform child in gameObject.transform) {
            Destroy(child.gameObject);
        }

        CreateLevel();
    }



    private void RepositonPlayer(){
        PlayerRef.transform.position = new Vector3(0, 4, 0);
    }

    private void CreateLevel(){
        //Generating level
        var StartBlock = Instantiate(BlockRef, transform.position, transform.rotation);
        StartBlock.GetComponent<Blocks>().starterBlock = true;
        blockPosList.Add(StartBlock.transform.position);

        StartBlock.transform.SetParent(this.gameObject.transform);

        while(BlockCount > 0){ //while there's blocks to be spawned, spawn them
            var NewBlock = Instantiate(BlockRef, transform.position, transform.rotation);
            
            float RandomizeLocX = Mathf.Round(Random.Range(-1f,1f));
            float RandomizeLocZ = Mathf.Round(Random.Range(-1f,1f));

            NewBlock.transform.position += new Vector3(RandomizeLocX, 0, RandomizeLocZ);

            while(blockPosList.Contains(NewBlock.transform.position)){ //check if block in this position has already been spawned, reposition if so
                NewBlock.transform.position += new Vector3(Mathf.Round(Random.Range(-2f,2f)), 0, Mathf.Round(Random.Range(-2f,2f)));
            }

            NewBlock.transform.SetParent(this.gameObject.transform);
            blockPosList.Add(NewBlock.transform.position);
            BlockCount--;
        }

        //Generating world tick entities
        if(EntityCount > 0){
            var StartEntity = Instantiate(EntityRef, transform.position, transform.rotation);
            Vector3 blockFromList = blockPosList[Random.Range(5, blockPosList.Count)];
            StartEntity.transform.position = new Vector3(blockFromList.x, 0, blockFromList.z);

            EntitiesToTick.Add(StartEntity);
            entityPosList.Add(StartEntity.transform.position);
            StartEntity.transform.position += new Vector3(0, 6f, 0);

            StartEntity.transform.SetParent(this.gameObject.transform);

            while(EntityCount > 0){ //while there's entities to be spawned, spawn them
                var NewEntity = Instantiate(EntityRef, transform.position, transform.rotation);
                blockFromList = blockPosList[Random.Range(5, blockPosList.Count)];

                NewEntity.transform.position = new Vector3(blockFromList.x, 0, blockFromList.z);

                while(entityPosList.Contains(NewEntity.transform.position)){ //check if entity in this position has already been spawned, reposition if so
                    blockFromList = blockPosList[Random.Range(5, blockPosList.Count)];
                    NewEntity.transform.position = new Vector3(blockFromList.x, 0, blockFromList.z);
                }

                NewEntity.transform.position += new Vector3(0, 6f, 0);

                NewEntity.transform.SetParent(this.gameObject.transform);
                EntitiesToTick.Add(NewEntity);
                entityPosList.Add(NewEntity.transform.position);
                EntityCount--;
            }
        }
    }

}
