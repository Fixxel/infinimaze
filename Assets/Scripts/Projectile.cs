using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public int DMG;
    public int TicksPassed;

    public List<GameObject> ThisType = new List<GameObject>();

    private void Start() {
        transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z));
    }

    public void Tick(){
        transform.Translate(0, 0, 1);
        transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z));

        //lifetime of a projectile
        TicksPassed++;
        if(TicksPassed >= 5){
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<Entity>()){
            other.GetComponent<Entity>().LoseHP(DMG);
            Destroy(gameObject);
            
        }else if(other.GetComponentInParent<Blocks>()){
            Destroy(gameObject);
        }
    }

}
