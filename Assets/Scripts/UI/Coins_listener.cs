using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coins_listener : MonoBehaviour
{

    public GameObject playerRef;
    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = ("Coins: " + playerRef.GetComponent<Player>().collectedCoins.ToString());
    }
}
