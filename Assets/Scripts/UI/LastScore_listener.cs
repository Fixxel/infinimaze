using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LastScore_listener : MonoBehaviour
{
    public GameObject LevelRef;
    public GameObject PlayerRef;
    public GameObject HighScoreRef;

    public void calculateScore(){
        int score = Mathf.RoundToInt(LevelRef.GetComponent<Level>().LevelNum * PlayerRef.GetComponent<Player>().collectedCoins);
        GetComponent<Text>().text = "Last Score Depth (" + LevelRef.GetComponent<Level>().LevelNum.ToString() + ") x Coins (" + PlayerRef.GetComponent<Player>().collectedCoins.ToString() + "): " + score.ToString();

        HighScoreRef.GetComponent<HighestScore_listener>().CompareScore(score);
    }
}
