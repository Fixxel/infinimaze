using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighestScore_listener : MonoBehaviour
{
    private int HighScore;
    public void CompareScore(int comparison){
        if(comparison > HighScore){
            HighScore = comparison;
            GetComponent<Text>().text = "HighScore: " + HighScore.ToString();
        }
    }
}
