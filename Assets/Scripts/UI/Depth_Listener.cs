using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Depth_Listener : MonoBehaviour
{

    public GameObject levelRef;
    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = ("Depth: " + levelRef.GetComponent<Level>().LevelNum.ToString());
    }
}
