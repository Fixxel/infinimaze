using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP_Listener : MonoBehaviour
{

    public GameObject playerRef;
    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = ("HP: " + playerRef.GetComponent<Player>().HP.ToString());
    }
}
