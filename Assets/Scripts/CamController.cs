using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{

    public float CameraRotationSpeed;
    public float CameraZoomSpeed;


    private float CameraRotationSpeedShifted;
    // Start is called before the first frame update
    void Start()
    {
        CameraRotationSpeedShifted = CameraRotationSpeed / 1.25f;

        transform.LookAt(Vector3.zero);
        transform.Translate(Vector3.back * 10f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Vector3.zero);

        //camera rotation
        if(Input.GetKey(KeyCode.Q)){
            transform.RotateAround(Vector3.zero, Vector3.up, CameraRotationSpeed * Time.deltaTime);
            if(Input.GetKey(KeyCode.LeftShift)){
                transform.RotateAround(Vector3.zero, Vector3.up, CameraRotationSpeedShifted * Time.deltaTime);
            }

        }else if(Input.GetKey(KeyCode.E)){
            transform.RotateAround(Vector3.zero, Vector3.down, CameraRotationSpeed * Time.deltaTime);
            if(Input.GetKey(KeyCode.LeftShift)){
                transform.RotateAround(Vector3.zero, Vector3.down, CameraRotationSpeedShifted * Time.deltaTime);
            }
        }

        //camera zooming
        if(Input.GetKey(KeyCode.Z)){
            if(Input.GetKey(KeyCode.LeftShift)){
                transform.Translate(Vector3.back * (CameraZoomSpeed * Vector3.Distance(transform.position, Vector3.zero)) * Time.deltaTime);
            }else{
                transform.Translate(Vector3.forward * (CameraZoomSpeed * Vector3.Distance(transform.position, Vector3.zero)) * Time.deltaTime);
            }
        }


        if(Vector3.Distance(transform.position, Vector3.zero) > 25f){
            transform.Translate(Vector3.forward * Time.deltaTime * 10f);
        }
    }
}
