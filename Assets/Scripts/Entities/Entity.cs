using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{

    public GameObject SensorForwardRef;
    public GameObject SensorBackRef;
    public GameObject SensorLeftRef;
    public GameObject SensorRightRef;
    public GameObject SensorPitfallRef;

    public List<GameObject> ThisType = new List<GameObject>();

    public int HP;
    private int GuardInvestigating_distance;

    private bool OrientationCornered;
    private bool LastRotationWasLeft;
    private bool GuardInvestigating;
    private enum Types {Wanderer, Guard}
    private Types thisType;

    private int DMG;
    private void Start() {
        if(Random.value < 0.7){
            thisType = Types.Wanderer;
            DMG = 1;
            HP = 1;
            ThisType[0].SetActive(true);
        }
        else{
            thisType = Types.Guard;
            DMG = 2;
            HP = 2;
            ThisType[1].SetActive(true);
        }
    }

    private void Update() {
        if(transform.position.y < -10f){
            Destroy(this.gameObject);
        }
    }

    public void Tick(){
        if(thisType == Types.Wanderer){
            WandererTick();
        }else if(thisType == Types.Guard){
            GuardTick();
        }
    }

    private void WandererTick(){
        if(Random.value > 0.5 && SensorPitfallRef.GetComponent<CollisionDetect>().collisionDetected){
            if(!OrientationCornered && !SensorForwardRef.GetComponent<CollisionDetect>().collisionDetected){
                transform.Translate(0, 0, 1f);
                transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z));
            }else if(OrientationCornered && !SensorForwardRef.GetComponent<CollisionDetect>().collisionDetected){
                transform.Translate(0, 0, 1.5f);
                transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z));
            }
        }else{
            if(Random.value > 0.5){
                transform.Rotate(0, -45f, 0);
                OrientationCornered = !OrientationCornered;
                LastRotationWasLeft = true;
            }else{
                transform.Rotate(0, 45f, 0);
                OrientationCornered = !OrientationCornered;
                LastRotationWasLeft = false;
            }
        }

        //rotate when facing a wall
        if(LastRotationWasLeft && SensorForwardRef.GetComponent<CollisionDetect>().collisionDetected){
            transform.Rotate(0, -45f, 0);
            OrientationCornered = !OrientationCornered;
        }else if(!LastRotationWasLeft && SensorForwardRef.GetComponent<CollisionDetect>().collisionDetected){
            transform.Rotate(0, 45f, 0);
            OrientationCornered = !OrientationCornered;
        }
    }

    private void GuardTick(){
        RaycastHit hit;
        bool WallBlock = false;

        if (!GuardInvestigating && Physics.Raycast(transform.position, transform.forward, out hit)){
            if(hit.transform.GetComponentInParent<Blocks>() != null){
                if(hit.transform.GetComponentInParent<Blocks>().SeeThrough == false) WallBlock = true; //is wall blocking raycast?
            }else{
                WallBlock = false;
            }

            if(hit.transform.GetComponent<Player>() && !WallBlock){
                print("Approacher spotted player!");
                GuardInvestigating = true;
                GuardInvestigating_distance = Mathf.RoundToInt(Vector3.Distance(transform.position, hit.transform.position));
            }
        }

        if(GuardInvestigating && GuardInvestigating_distance > 0){
            transform.Translate(0, 0, 1f);
            if(OrientationCornered) transform.Translate(0, 0, 0.5f);
            transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z));
            
            GuardInvestigating_distance--;
            if(GuardInvestigating_distance <= 0){
                GuardInvestigating = false;
            }
        }
        else if(Random.value > 0.5){
            transform.Rotate(0, -45f, 0);
            OrientationCornered = !OrientationCornered;
            LastRotationWasLeft = true;
        }else{
            transform.Rotate(0, 45f, 0);
            OrientationCornered = !OrientationCornered;
            LastRotationWasLeft = false;
        }

    }


    public void LoseHP(int amount){
        HP -= amount;
        if(HP <= 0){
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<Player>()){ //damaging player
            other.GetComponent<Player>().LoseHP(DMG);
            if(thisType == Types.Guard){
                Destroy(gameObject);
            }
        }
    }
}
